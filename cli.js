#!/usr/bin/env node
const prompts = require('prompts')
const process = require('process')
const fs = require('fs-extra')
const { exec, spawn } = require('child_process');

/*
 * Replace file content
 * @param {String} file
 * @param {Object} data
 */
function setupFile(file, data) {
    let content = fs.readFileSync(file, 'utf8')
    for(let id in data) {
        content = content.replace(id, data[id])
    }
    fs.writeFileSync(file, content, 'utf8')
}

// Welcome
console.log('DanFw v4 (alpha) | Starting setup your project...')

// Setup
prompts([
    {
        type: 'text',
        name: 'title',
        message: 'What is your project title ?',
        validate: value => value.length < 1 ? 'A project title is required' : true
    },
    {
        type: 'text',
        name: 'description',
        message: 'Describe your project in few words',
        validate: value => value.length < 1 ? 'A description is required' : true
    },
    {
        type: 'list',
        name: 'locales',
        message: 'Enter at least one locale (default in first)',
        initial: 'en, fr',
        separator: ','
    },
    {
        type: 'select',
        name: 'git',
        message: 'Initialize git ?',
        choices: [
            { title: 'Yes', value: true },
            { title: 'No', value: false }
        ],
        initial: 0
    }
]).then((data) => {
    if(!data.title) {
        return;
    }

    const projectPath = process.cwd()
    
    console.log('🚚  Copying template files...')

    /**
     * Remove template dependencies & dist
     */
    fs.removeSync(__dirname+'/template/node_modules')
    fs.removeSync(__dirname+'/template/.next')

    /*
     * Copy
     */
    fs.copySync(__dirname+'/template', projectPath)

    /*
     * Setup
     */
    console.log('🕹️   Setup your project...')
    
    // Title & description
    setupFile(projectPath+'/Readme.md', {
        'project-name': data.title,
        'project-description': data.description
    })
    setupFile(projectPath+'/src/pages/index.js', {
        'project-name': data.title,
        'project-description': data.description
    })
    setupFile(projectPath+'/package.json', {
        'project-name': data.title.toLowerCase().replace(/ /g, '-'),
        'project-description': data.description
    })

    // Locales
    const localesPath = projectPath+'/public/locales/_locale'
    fs.copySync(projectPath+'/public/locales/en', localesPath)
    data.locales.forEach(locale => {
        fs.copySync(localesPath, projectPath+'/public/locales/'+locale)
    })
    fs.removeSync(localesPath)

    setupFile(projectPath+'/.env', {
        'LOCALES = en': 'LOCALES = '+data.locales.join(', ')
    })

    // Git
    if(data.git) {
        console.log('🌎  Setup git')
        exec('git init "'+projectPath+'"')
    }
    
    // Git ignore
    fs.moveSync(projectPath+'/gitignore', projectPath+'/.gitignore')
    
    /*
     * Dependencies
     */

    console.log('🚀  Install dependencies...')

    // Windows support
    const extention = process.platform.search(/win[0-9]{2}/) !== -1?'.cmd':''

    // Run
    ls = spawn('npm'+extention, ['i'])
    
    ls.stdout.on('data', function (data) {
        console.log(data.toString())
    })

    ls.stderr.on('data', function (data) {
        console.log(data.toString())
    })
    
    // Start
    ls.on('exit', function (code) {
        // Complete
        console.log("🎉  Your project is ready !\nYou can run it with: npm run dev\nDocumentations and commands are available in the Readme.md")
    })
})