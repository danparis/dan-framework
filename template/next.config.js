const withPlugins = require('next-compose-plugins')
const getConfig = require('./src/config')
const config = getConfig()

const scss = require('./server/scss')
const analyze = require('./server/analyze')
const alias = require('./server/alias')
const livereload = require('./server/livereload')

module.exports = withPlugins([
    scss,
    analyze,
], {
    // Webpack
    webpack(config, options) {
        alias(config.resolve.alias)
        return config
    },
    
    // Public config
    publicRuntimeConfig: config,
    serverRuntimeConfig: getConfig.getServerConfig(),

    // CDN
    assetPrefix: config.assetsCdn,

    // Dev server
    webpackDevMiddleware: (config) => {
        livereload(config)
        return config
    }
})