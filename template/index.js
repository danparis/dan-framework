const express = require('express')
const next = require('next')
const routes = require('./src/routes')
const config = require('./src/config')()
const log = require('./server/log')
const authorization = require('./server/authorization')

const app = next({
    dev: config.isDev
})
const handler = routes.getRequestHandler(app)

app.prepare().then(() => {
    const server = express()

    // Authenticate
    authorization(server)

    // Next
    server.use(handler).listen(config.port, () => {
        log(`listening port _${config.port}_`, 'server')
    })
})