const config = require('../src/config')()
const shorthash = require('shorthash')

function getIdent(context, localIdentName, localName, options) {
    return context.resourcePath
        .replace(context.rootContext, '')
        +'-'+localName
}

module.exports = [
    require('@zeit/next-sass'), {
        cssModules: true,
        cssLoaderOptions: {
            importLoaders: 1,
            getLocalIdent: (context, localIdentName, localName, options) => {
                const id = getIdent(context, localIdentName, localName, options)
                return '.'+shorthash.unique(id)+(config.isDev?id:'')
            }
        },
        sassLoaderOptions: {
            data: '@function assets($url) { @return url("'+config.assets+'"+$url); }',
        },
    }
]