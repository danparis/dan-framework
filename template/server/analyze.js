const config = require('../src/config')()

module.exports = [
    require('@next/bundle-analyzer')({
        enabled: config.analyze
    })
]