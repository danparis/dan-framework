const routes = require('next-routes')()
const config = require('../src/config')()
const log = require('./log')

/**
 * Clean a rout path
 * @param {String} path 
 * @returns {String}
 */
function cleanPath(path) {
    path = path
        .replace(/\/\//g, '/')
        .replace(/\/$/, '')
    if(path === '') {
        path = '/'
    }
    return path
}

/**
 * Add a new localized route path
 * @param {String} path Route path
 * @param {String} page Page name
 */
function addRoute(path, page, name) {
    // Setup route
    if(path.indexOf(path.length - 1) !== '/') {
        path += '/'
    }
    path = cleanPath(path.replace(new RegExp(`^/(${config.locales.join('|')})/`), '/:locale($1)/'))
    //config.isServer && log(`_${path}_ -> *${name}*`, 'router') // Log routes
    routes.add(name, path, page)
}

/**
 * Add a new route for all locales
 * @param {String} path 
 * @param {String} page 
 * @param {String} name 
 */
function addI18nRoute(path, page, name) {
    path = cleanPath(`/:locale(${config.locales.join('|')})?${path}`)
    addRoute(path, page, name)
}

module.exports = {
    routes,
    addRoute,
    addI18nRoute
}