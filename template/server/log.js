const chalk = require('chalk')

const COLOR_MAP = {
    ready: chalk.blue,
    event: chalk.magenta,
    wait: chalk.cyan,
}

/**
 * Format data
 * @param {String} data 
 * @returns {String}
 */
function format(data) {
    return data
        .replace(/_(.*?)_/g, chalk.green('$1'))
        .replace(/\*(.*?)\*/g, chalk.magenta('$1'))
}

/**
 * Log message
 */
module.exports = (message, status) => {
    console.log(
        '[ '+
        (COLOR_MAP[status]?COLOR_MAP[status](status):chalk.blue(status))+
        ' ] '+
        format(message)
    )
}