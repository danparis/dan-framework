module.exports = (config) => {
    config.watchOptions = {
        poll: 250, // ms
        aggregateTimeout: 50, // ms
    }
}
