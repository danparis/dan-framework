const fetch = require('isomorphic-unfetch')
const config = require('../src/config')()

/**
 * Generate robots.txt
 */
async function getRobot() {
    // With sitemap API
    return await (await fetch('http://bo/api/robots.txt', {
        headers: {
            Authorization: 'Basic abcdefghijklmnopqrstuvwzyz'
        }
    })).text()
}

/**
 * Add /robots.txt to the server
 */
module.exports = (app) => {
    app.get('/robots.txt', async (req, res) => {
        try {
            res.status(200).type('text/plain; charset=UTF-8').end(await getRobot())
        }
        catch(error) {
            res.status(500).end('Internal error')
        }
    })
}
