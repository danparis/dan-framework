const log = require('./log')
const { authLogin, authPassword } = require('../src/config').getServerConfig()

/*
 * Basic auth
 */
function basicAuth(req, res, next) {
    const authorization = req.header('Authorization') || req.header('Proxy-Authorization')
    const basicAuth = 'Basic '+Buffer.from(authLogin+':'+authPassword).toString('base64')

    if(authorization === basicAuth) {
        next()
    }
    else {
        res.set("WWW-Authenticate", "Basic realm=\"Authorization Required\"")
        res.status(401).send("Authorization Required")
    }
}

/**
 * Setup authentification
 */
module.exports = (server) => {
    if(authLogin && authPassword) {
        server.use(basicAuth)
        log('basic Auth _On_')
    }
}