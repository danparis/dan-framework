const path = require('path')

module.exports = (alias) => {
    // Alias
    alias.app = path.join(__dirname, '../src/app')
    alias.config = path.join(__dirname, '../src/config')
    alias.dan = path.join(__dirname, '../src/dan')
    alias.signals = path.join(__dirname, '../src/signals')
    
    // Libs
    alias.signal = 'mini-signals'
}