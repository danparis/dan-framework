const fetch = require('isomorphic-unfetch')
const config = require('../src/config')()
const lastmod = new Date()

/**
 * Generate sitemap.xml
 */
async function getSitemap() {
    /*
    // With sitemap API
    return await (await fetch('http://bo/api/sitemap.xml', {
        headers: {
            Authorization: 'Basic abcdefghijklmnopqrstuvwzyz'
        }
    })).text()
    */

    // Generated by code
    return `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
    ${
        [
            { loc: '/', changefreq: 'weekly', lastmod },
            { loc: '/a', changefreq: 'monthly', lastmod },
            { loc: '/b/c', changefreq: 'monthly', lastmod },
        ].map(({ loc, lastmod, changefreq }) => ` <loc>${config.basePath}${loc}</loc>
    <lastmod>${lastmod.toISOString()}</lastmod>
    <changefreq>${changefreq}</changefreq>
    <priority>${Max.max(.1, 1 - (loc.split('/').length - 2) * .1)}</priority>
`
        ).join(`</url><url>`)
    }
</url>
</urlset>`
}

/**
 * Add /sitemap.xml to the server
 */
module.exports = (app) => {
    app.get('/sitemap.xml', async (req, res) => {
        try {
            res.status(200).type('text/xml; charset=UTF-8').end(await getSitemap())
        }
        catch(error) {
            res.status(500).end('Internal error')
        }
    })
}
