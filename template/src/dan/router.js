import Routes from '../routes'
import { useI18n } from './i18n'

// Router
export const Router = Routes.Router

/**
 * <Link />
 */
export function Link(props) {
    const data = {...props}

    const i18n = useI18n()

    if(i18n) {
        data.params = data.params || {}
        data.params.locale = data.params.locale || i18n.locale
    }

    return (
        <Routes.Link {...data}/>
    )
}

/**
 * Push a new route in the router
 */
export function pushRoute(route, params = null, options = null) {
    // With params or options
    if(params || options) {
        params = params || {}
        const data = {...params}

        const i18n = useI18n()

        if(i18n) {
            data.params = data.params || {}
            data.params.locale = data.params.locale || i18n.locale
        }

        Router.pushRoute(route, data, options)
    }
    // With href
    else {
        Router.pushRoute(route)
    }
}