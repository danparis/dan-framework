import css from './index.scss'
import Head from 'next/head'

import Readme from 'app/components/readme'

/**
 * Home page
 */
function HomePage() {
    return (
        <div className={css.component}>
            <Head>
                <title>project-name</title>
                <meta name="description" content="project-description" />
            </Head>

            <h1>Hello Dan</h1>
            <Readme />
        </div>
    )
}

export default HomePage