import App from 'next/app'
import css from './_app.scss'

// App
function _App(props) {
    const { Component, pageProps } = props
    
    return (
        <div className={css.app}>
            <Component {...pageProps} />
        </div>
    )
}

/* Only uncomment this method if you have blocking data requirements for
 every single page in your application. This disables the ability to
 perform automatic static optimization, causing every page in your app to
 be server-side rendered. */
// _App.getInitialProps = async (appContext) => {
//   const appProps = await App.getInitialProps(appContext)
//   return { ...appProps }
// }

export default _App