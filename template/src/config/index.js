const SPLIT_ARRAY = / ?, ?/

// Config
const config = {} // Public
const serverConfig = {} // Server

// Config from ENVs
if(typeof window === 'undefined') {
    require('dotenv').config()
    
    // Port
    const port = parseInt(process.env.PORT, 10) || 3000

    // Envs
    config.isServer = true
    config.port = port
    config.isDev = process.env.NODE_ENV === 'development'
    config.appEnv = process.env.APP_ENV || 'developement'
    config.basePath = process.env.BASE_PATH || `http://localhost:${port}`
    config.assets = process.env.ASSETS_PATH || '/'
    config.locales = (process.env.LOCALES || 'en').split(SPLIT_ARRAY)
    config.locale = process.env.LOCALE || config.locales[0]
    config.analyze = process.env.ANALYZE_BUILD === 'true'
    config.assetsCdn = process.env.NEXT_PATH || ''
    config.cacheI18nClient = true
    config.cacheI18nServer = !config.isDev

    // Server
    serverConfig.authLogin = process.env.AUTH_LOGIN || null
    serverConfig.authPassword = process.env.AUTH_PASSWORD || null
}

// Public
module.exports = () => {
    if(typeof window === 'undefined') {
        return config
    }
    else {
        const getConfig = require('next/config').default
        getConfig().publicRuntimeConfig.isServer = false
        return getConfig().publicRuntimeConfig
    }
}

// Server
module.exports.getServerConfig = () => {
    if(typeof window === 'undefined') {
        return serverConfig
    }
    else {
        const getConfig = require('next/config').default
        return getConfig().serverRuntimeConfig
    }
}
