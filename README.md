# DAN Fw v4

A NextJS starter

## Installation

On `mac` :

```
npm init dan
```

On `windows`:

```
npx create-dan
```

## Documentation & Commands

More informations in [./template/README.md](template/README.md)

## v3

### Installation

On `mac` :

```
npm init dan@3
```

On `windows`:

```
npx create-dan@3
```